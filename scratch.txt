Trendnote Object Model

Trend Details
{
	source: "Instagram or Twitter or *",
	user: "User name, readable",
	handle: "Twitter or instagram handle",
	pic: "The users profile picture"
	time: "Time of Tweet or Instagram",
	trend: "The related trending tag (twitter)",
	favorites: "Favorites (twitter) or likes (instagram)",
	retweets: Retweets if twitter""
	text: "Text of the tweet or instagram",
	mediaUrl: "The image (or video) attached to the tweet"

}


public TrendNotesMainAdapter(){
        Trendnote sampleTrendnote = new Trendnote();
        sampleTrendnote.setUser("Ubani Balogun");
        sampleTrendnote.setHandle("@ubaniabalogun");
        sampleTrendnote.setFavorites("10000");
        sampleTrendnote.setRetweets("10000");
        sampleTrendnote.setTime("2m");
        sampleTrendnote.setText("Download by new app #Working");
        trendnotes.add(sampleTrendnote);
        trendnotes.add(sampleTrendnote);
        trendnotes.add(sampleTrendnote);
    }

    @Override
    public int getCount() {
        return trendnotes.size();
    }

    @Override
    public Object getItem(int i) {
        return getItem(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            view = layoutInflater.inflate(R.layout.trendnote_cell,viewGroup,false);
        }
        Trendnote note = trendnotes.get(i);

        ImageView picView = (ImageView)view.findViewById(R.id.trendnote_pic);
        picView.setImageResource(R.drawable.ic_launcher);

        TextView userTextView = (TextView)view.findViewById(R.id.trendnote_user);
        userTextView.setText(note.getUser());

        TextView handleView = (TextView)view.findViewById(R.id.trendnote_handle);
        handleView.setText(note.getHandle());

        TextView timeView = (TextView)view.findViewById(R.id.trendnote_date);
        timeView.setText(note.getTime());

        TextView textView =  (TextView)view.findViewById(R.id.trendnote_text);
        textView.setText(note.getText());

        ImageView mediaView = (ImageView)view.findViewById(R.id.trendnote_media);
        mediaView.setImageResource(R.drawable.ic_launcher);

        return view;
    }