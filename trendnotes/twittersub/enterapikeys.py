"""
twittersub.apikeys.py
@Author: Ubani Anthony Balogun
@Date: June 17, 2014

Twitter API keys

Create a demo application on Twitter and fill in the approriate keys here. Then rename the file to apikeys.py 
"""

TWITTER = {}
TWITTER['CONSUMER_KEY'] = ''
TWITTER['CONSUMER_SECRET'] = ''
TWITTER['ACCESS_TOKEN'] = ''
TWITTER['ACCESS_TOKEN_SECRET'] = ''