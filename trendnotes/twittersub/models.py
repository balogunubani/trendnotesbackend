"""
twittersub.models.py
@Author: Ubani Anthony Balogun
@Date: June 14, 2014

This file contains the models and operations for subsetting Twitter API
"""

from .apikeys import TWITTER
import tweepy

class ApiWrapper():
	"""ApiWrapper for the class."""
	twconsumer_key = TWITTER['CONSUMER_KEY']
	twconsumer_secret = TWITTER['CONSUMER_SECRET']
	twaccess_token = TWITTER['ACCESS_TOKEN']
	twaccess_token_secret = TWITTER['ACCESS_TOKEN_SECRET']
	twauth = tweepy.OAuthHandler(twconsumer_key,twconsumer_secret)
	twauth.set_access_token(twaccess_token,twaccess_token_secret)
	twapi = tweepy.API(twauth)

class TwitterSub(ApiWrapper):
	"""Provides methods for querying the Twitter Api and subsetting response."""

	def __init__(self):
		pass

	def get_trends(self,woeid=1,exclude=None,fields=[]):
		"""
		Gets trends from twitter by place.

		Parameters:
		-----------
		woeid : int
			The Yahoo WOEID of the place
		exclude : str
			Set to 'hashtags' to exclude tweets with hashtags

		Returns:
		--------
		trends : list
			a list of trends dictionaries from twitter

		"""
		if exclude == 'hashtags':
			raw = self.twapi.trends_place(id=woeid,exclude=exclude)
		else:
			raw = self.twapi.trends_place(id=woeid)
		trends = raw[0]['trends']

		return trends

	def get_notes(self,query,fields,result_type='mixed',count='20'):
		"""
		Gets the notes related to the trends (in this case, tweets).
		
		Parameters:
		-----------
		query : str
			The query string for the trend
		result_type : str
			Set to 'popular' to return only popular tweets. Set to 'mixed' return popular and real time results
		fields : list
			The desired fields to be extracted from the default query result, e.g. ['favorite_count','text','author']

		Returns:
		--------
		tweets : list
			The list of tweet dictionaries with only the data specified in fields array 

		Notes:
		------
		Nested fields can be accessed 

		"""
		default = ''
		search = self.twapi.search(q=query,result_type=result_type, count=count)
		tweets = []
		fields = self.preprocess_fields(fields)
		for status in search:
			tweet = {}
			tweet['query'] = query
			tweet['source'] = 'twitter'
			for field in fields:
				if isinstance(field,tuple):
					key, value = field
					temp = getattr(status,key)
					if key not in tweet:
						tweet[key] = {}
					tweet[key][value] = getattr(temp,value,default)
				else:
					tweet[field] = getattr(status,field,default)
			tweets.append(tweet)
		return tweets

	@staticmethod
	def preprocess_fields(fields):
		"""
		A helper method for preprocessing the parameter fields used by the method get_notes.
		The preprocessing allows the method get_notes to easily access nested objects.

		Parameters:
		-----------
		fields : list
			The list of fields to be preprocessed

		Returns:
		--------
		preprocessed_fields : list
			A list containing tuples and strings 
		"""
		preprocessed_fields = []
		for field in fields:
			if "-" in field:
				preprocessed_fields.append(tuple(field.split('-')))
			else:
				preprocessed_fields.append(field)
		return preprocessed_fields












