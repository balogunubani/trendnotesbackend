"""
twittersub.twitter_tasks.py 
@Author: Ubani Anthony Balogun
@Date: June 15, 2014

This file contains the celery tasks that will be perfomed
"""

from trendnotes import app, celery
from extensions import mongo
from trendnotes.twittersub.models import TwitterSub
import random


@celery.task(name='twitter_tasks.fetch_twitter_trends')
def fetch_twitter_trends():
	twitter_fields = ['user-name','user-screen_name','user-profile_image_url_https','created_at','text','favorite_count','retweet_count','entities-media','id_str']
	twittersub = TwitterSub()
	trends = twittersub.get_trends(23424977)
	trendnotes = []
	for each in trends:
		trend = each['query']
		notes = twittersub.get_notes(query=trend,fields=twitter_fields,result_type='popular',count='5')
		trendnotes.extend(notes)

	random.shuffle(trendnotes)
	mongo.db.trendnotes.insert(trendnotes)
	