"""
instapy.py

@author: Ubani Balogun
@date: 08/23/2014

My client class for the Instagram API 
"""
import requests

class InstaPy():
	"""My client for the Instagram api. Uses python requests module to parse the raw json responses"""

	host = 'api.instagram.com'
	version = '/v1'
	protocol = 'https'

	def __init__(self,client_id,client_secret):
		self.client_id = client_id
		self.client_secret = client_secret
		self.base_path = ''.join([InstaPy.protocol,'://',InstaPy.host,InstaPy.version])


	def media_popular(self,count='20'):
		"""
		Get a list of what media is most popular at the moment. Can return mix of image and video types.

		Parameters:
		-----------
		Count: str
			The number of results to return

		Returns:
		--------
			popular_media: list
				a list of popular media dictionaries
		"""
		endpoint = '/media/popular'
		url = ''.join([self.base_path,endpoint,'?','client_id','=',self.client_id,'&','count','=',count])
		try:
			r = requests.get(url)
			popular_media = r.json()
		except Exception, e:
			print str(e)
			popular_media = {}
		return popular_media





