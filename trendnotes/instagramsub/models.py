"""
instagramsub.models.py 

@Author: Ubani Anthony Balogun
@Date: June 17, 2014

This file contains the module for subsetting the Instagram API 
"""
from instagram.client import InstagramAPI
from .apikeys import INSTAGRAM
from .instapy import InstaPy



class ApiWrapper():
	"""API wrapper for the class."""
	instaclient_id =  INSTAGRAM['CLIENT_ID']
	instaclient_secret = INSTAGRAM['CLIENT_SECRET']
	instapi = InstaPy(client_id=instaclient_id,client_secret=instaclient_secret)


class InstagramSub(ApiWrapper):
	"""Provides methods for querying and subsetting the Instagram API"""

	def __init__(self):
		pass

	def get_trends(self,fields,functions={},count='20'):
		"""
		Gets trends (popular media) from Instagram
		
		Parameters
		----------
		fields: list
			The desired fields to be returned from the query result 
		count : int
			Optional paramater indicating the number of results to return

		Returns:
		--------
		popular_media: list
			A list of popular media dictionaries with only the data specified in fields

		"""
		count = str(count)
		fields = self.preprocess_fields(fields)
		raw = self.instapi.media_popular(count=count)
		popular_media = []
		if not raw:
			return raw

		for data in raw['data']:
			media = {}
			media['source'] = 'instagram'
			for field in fields:
				if isinstance(field,tuple):
					parent,child = field
					if not media.has_key(parent):
						media[parent] = {}
					try:
						media[parent][child] = data.get(parent).get(child)
					except Exception, e:
						print str(e)
						media[parent][child] = ""
				else:
					try:
						media[field] = data.get(field)
					except Exception,e:
						print str(e)
						media[field] = data.get(field)

			popular_media.append(media)


		return popular_media


	@staticmethod
	def process_fields(raw):
		"""
		Process and standardize the fields returned from the Instagram API

		Parameters:
		-----------
		fields: list
			The fields to extract from the raw data
		raw: dict
			The json response from the Instagram API

		Returns:
		--------
		result: dict
			A dictionary of fields that define a trendnote
		"""
		results = []
		

	@staticmethod
	def preprocess_fields(fields):
		"""
		A helper method for preprocessing the parameter fields used by the method get_notes.
		The preprocessing allows the method get_notes to easily access nested objects.

		Parameters:
		-----------
		fields : list
			The list of fields to be preprocessed

		Returns:
		--------
		preprocessed_fields : list
			A list containing tuples and strings 
		"""
		
		if not fields:
			return fields

		preprocessed_fields = []
		for field in fields:
			if "-" in field:
				preprocessed_fields.append(tuple(field.split('-')))
			else:
				preprocessed_fields.append(field)
		return preprocessed_fields