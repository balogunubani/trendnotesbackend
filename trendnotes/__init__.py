"""
__init__.py for trendnotes package
@Author: Ubani Anthony Balogun
@Date: June 15, 2014
"""
from flask import current_app as app

# Celery
celery = app.celery 

def register_api(cls, *urls, **kwargs):
	""" A simple pass through class to add entities to the api registry """
	kwargs["endpoint"] = getattr(cls, 'resource_name', kwargs.get("endpoint", None))
	app.api_registry.append((cls, urls, kwargs))