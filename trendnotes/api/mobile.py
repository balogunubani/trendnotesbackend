"""
mobile.py
@author: Ubani Balogun
@date: August 13, 2014

Mobile endpoints for the iOS and Android app
"""

from flask import jsonify, make_response, Blueprint
from flask.ext.restful import Resource, reqparse
import urllib
from trendnotes import register_api
from trendnotes.twittersub.models import TwitterSub
from trendnotes.instagramsub.models import InstagramSub
from extensions import mongo
import random

class MobileTrendnotesAPI(Resource):
	""" Endpoint for Returning Instagram and Twitter Trend details """

	resource_name =  "mobile_trendnotes"

	def get(self):
		trendnotes = []
		collection = mongo.db.trendnotes
		for note in collection.find(fields={'_id':False}):
			trendnotes.append(note)
		return jsonify({'trendnotes':trendnotes})


register_api(MobileTrendnotesAPI,'/mobile/all')