"""
trendnotes.api.instagramblue.py
@Author: Ubani Anthony Balogun
@Date: June 18, 2014

Blueprint for instagram subclass
"""
from flask import Blueprint, jsonify
from flask.ext.restful import Resource, reqparse
import urllib
from trendnotes import register_api
from trendnotes.instagramsub.models import InstagramSub


class InstagramTrendAPI(Resource):
	""" Instagram API resource for fetching trends """

	resource_name = "instagram_trends"

	def get(self):
		instagramsub = InstagramSub()
		fields = ['id','user-full_name','user-username','user-profile_picture','created_time','caption-text','link']
		functions = {'image' : 'get_standard_resolution_url'}
		trends = instagramsub.get_trends(fields,functions,count=1)
		return jsonify({'trends':trends})


class InstagramDetailAPI(Resource):
	""" Instagram API resource for fetching media by tags """

	resource_name = "instagram_details"

	def get(self,trend):
		return "Nothing Here"

register_api(InstagramTrendAPI,'/instagram')
register_api(InstagramDetailAPI,'/instagram/<string:trend>')