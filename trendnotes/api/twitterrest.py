"""
twitterrest.py
@author: Ubani Balogun
@date: July 22, 2014

Twitter API restful resource
"""

from flask import jsonify, make_response, Blueprint
from flask.ext.restful import Resource, reqparse
import urllib
from trendnotes import register_api
from trendnotes.twittersub.models import TwitterSub

class TwitterTrendAPI(Resource):
	"""Twitter api resource for returning trends"""

	resource_name = "twitter_trends"

	def get(self,woeid):
		twittersub = TwitterSub()
		trends = twittersub.get_trends(woeid)
		return jsonify({'trends':trends})


class TwitterDetailAPI(Resource):
	"""Twitter Api resource for returning trend related details i.e tweets"""

	resource_name = "twitter_details"

	def get(self,woeid,trend):
		fields = ['text','id_str','favorite_count','retweet_count','user-id_str','user-screen_name','user-name','user-profile_image_url_https','created_at','entities']
		twittersub = TwitterSub()
		tweets = twittersub.get_notes(trend,fields)
		return jsonify({'trendnotes':tweets})

register_api(TwitterTrendAPI,'/twitter/<int:woeid>')
register_api(TwitterDetailAPI,'/twitter/<int:woeid>/<string:trend>')



