"""
run.py 
@Author: Ubani Balogun
@Date: June 9, 2014

This file starts the application server
"""

from factory import create_app, register_blueprints, initialize_api


app = create_app('trendnotes','config.Config')

with app.app_context():
	from trendnotes.api.twitterrest import TwitterTrendAPI, TwitterDetailAPI
	from trendnotes.api.instagramrest import InstagramTrendAPI, InstagramDetailAPI
	from trendnotes.api.mobile import MobileTrendnotesAPI
	from extensions import api
	initialize_api(app,api)
	app.run()