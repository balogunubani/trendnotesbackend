"""
extensions.py

@Author: Ubani Anthony Balogun
@Date

This file initializes the application extensions 
"""

from flask.ext.pymongo import PyMongo
from flask.ext import restful

# Flask-PyMongo extension instance
mongo = PyMongo()

# Flask-Restful extension instance
api = restful.Api(prefix='/api/1')
