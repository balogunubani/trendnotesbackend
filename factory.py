"""
factory.py

@Author: Ubani Balogun
@Date: June 7, 2014

Factory methods for the application
@TODO: write an initialize_api function
"""
from flask import Flask
from celery import Celery

def initialize_api(app, api):
	""" Register all resources for the API """
	_resources = getattr(app, "api_registry", None)
	if _resources and isinstance(_resources, (list, tuple,)):
		for cls, args, kwargs in _resources:
			api.add_resource(cls, *args, **kwargs)
	api.init_app(app=app) # Initialize api first
	
def register_blueprints(app, *blueprints):
	"""
	Register blueprints for the application
	"""
	for blueprint in blueprints:
		app.register_blueprint(blueprint)


def create_app(app_name,config_obj):
	"""
	Generates and configures the application and its extensions
	"""
	# Launch Application
	app = Flask(app_name)

	# Load the application configuration
	app.config.from_object(config_obj)

	# Initialize pymongo
	from extensions import mongo
	mongo.init_app(app)

	# Initialize the api registry
	from extensions import api
	api.init_app(app)

	# Initialize celery app
	app.celery = make_celery(app)


	app.api_registry = []
	return app


def make_celery(app):
	"""Creates the celery app and enables it to run in the flask context """
	celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
	celery.conf.update(app.config)
	TaskBase = celery.Task
	class ContextTask(TaskBase):
		abstract = True
		def __call__(self, *args, **kwargs):
			with app.app_context():
				return TaskBase.__call__(self, *args, **kwargs)
	celery.Task = ContextTask
	return celery