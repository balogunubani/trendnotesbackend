# TrendNotes
TrendNotes is a web and mobile application that correlates social media trends with online news articles. It is an experimental project designed to explore the process of:

- Querying and subsetting the Twitter and Instagram APIs
- Developing algorithms to determine relationships between social media trends, Google search results and online articles
- Developing back-end APIs for mobile and web applications using the Flask Framework and MongoDB
- Having fun while creating software
