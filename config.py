"""
config.py 
@Author: Ubani Anthony Balogun
@Date: June 7, 2014

Configuration classes for the application
"""

from datetime import timedelta

class Config(object):
	""" Base configuration for the application """
	DEBUG = True
	# Mongodb Configurations
	MONGO_HOST = 'localhost'
	MONGO_PORT = 27017
	MONGO_DBNAME = 'trendnotesdb'

	#Celery Configurations
	CELERY_BROKER_URL = 'amqp://'



	CELERYBEAT_SCHEDULE = {
		'fetch-twitter-trends': {
			'task': 'twitter_tasks.fetch_twitter_trends',
			'schedule': timedelta(seconds=60),
		}
	}

	CELERY_TIMEZONE = 'UTC'
	
