"""
tasks.py 
@author: Ubani Balogun
@date: 08/21/2014

Main Celery tasks file
"""

from factory import create_app

app = create_app('trendnotes','config.Config')

with app.app_context():
	from trendnotes import celery
	from trendnotes.twittersub import twitter_tasks
	